# 病毒存储库
你好！这里是我视频中的病毒存储的地方，如果你想下载，可以从这里下载。

由于网络问题，请在下载文件前检查Github和Gitee的提交次数(Commits)来保证最新版。
### 注意
该库中的病毒**非常危险**（除了后缀带-Clean的文件），如果你想测试，请使用虚拟机的网页浏览器下载。所有的压缩包密码是：**virus**，如果解压密码出现错误，请通过Issues反馈，谢谢。

**我不会对存储库中的病毒以及你的疏忽造成的任何错误负责。**
